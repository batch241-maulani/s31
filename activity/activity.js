// Part 1

// 1. What directive is used by Node.js in loading the modules it needs?

	-It uses the require method to load the module it needs.

// 2. What Node.js module contains a method for server creation?

	-http module contains the method for server creation

// 3. What is the method of the http object responsible for creating a server using Node.js?

	- .createServer() is the method responsible for creating a server in Node.js

// 4. What method of the response object allows us to set status codes and content types?

	- .writeHead() allows us to set status codes and content types

// 5. Where will console.log() output its contents when run in Node.js?

	- the console.log of the Node.js goes directly to the terminal, not the console in the browser

// 6. What property of the request object contains the address's endpoint?

	.url contains the address endpoint
